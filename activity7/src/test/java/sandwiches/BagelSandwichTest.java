package sandwiches;

import static org.junit.Assert.*; 
import org.junit.Test;

public class BagelSandwichTest {
    
    @Test 
    public void constructorTest(){
        BagelSandwich bs = new BagelSandwich();

        assertEquals("", bs.getFilling());
    }

    @Test
    public void addFillingTest(){
        BagelSandwich bs = new BagelSandwich();

        bs.addFilling("creamcheese");
        assertEquals("creamcheese", bs.getFilling());

        bs.addFilling("bacon");
        assertEquals("creamcheese, bacon", bs.getFilling());
    }

    @Test (expected = UnsupportedOperationException.class)
    public void isVegetarianTest(){
        BagelSandwich bs = new BagelSandwich();
        bs.isVegetarian();
    }
}
