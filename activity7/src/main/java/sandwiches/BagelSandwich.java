package sandwiches;

public class BagelSandwich implements ISandwich{
    
    private String filling;
    
    public BagelSandwich(){
        this.filling = "";
    }
    
    public String getFilling(){
        return this.filling;
    }

    public void addFilling(String topping){
        throw new UnsupportedOperationException();
    }

    public boolean isVegetarian(){
        throw new UnsupportedOperationException();
    }
}
