package sandwiches;

public class VegetarianSandwich implements ISandwich{
    private String filling;

    public VegetarianSandwich(){
        this.filling = "";
    }

    public String getFilling(){
        return this.filling;
    }

    public void addFilling(String topping){
        String[] nonVeget = {"chicken", "beef", "fish", "meat", "pork" };
        for (int i = 0; i < nonVeget.length; i++){
            if (topping.equals(nonVeget[i])){
                throw new IllegalArgumentException("No meat");
            }
        }
        if (this.filling == ""){
            this.filling = topping;
        } else {
            this.filling += ", " + topping;
        }
    }

    public boolean isVegetarian(){
        return true;
    }

    public boolean isVegan(){
        String[] fillings = this.filling.split(", ");
        for (int i = 0; i < fillings.length; i++){
            if (fillings[i].equals("egg") || fillings[i].equals("cheese")){
                return false;
            }
        }
        return true;
    }
}   